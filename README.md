# Wikhistoric

Wikhistoric aims at auto creating an historic website that says who did what to what, when, for the world to know it.

The whole idea is to get a single place where to browse everything. Sources should be traceable, editable, commentable, etc.

Stack : vue.js / firebase / python (for the data science stuff)